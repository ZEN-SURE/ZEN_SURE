const cloud = require("wx-server-sdk");

exports.main = async (event) => {
  cloud.init({
    env: cloud.DYNAMIC_CURRENT_ENV,
  });

  const db = cloud.database();

  const fn = actions[event.action];
  if (!fn) throw new Error("no such action " + event.action);
  return fn(event.req);
};

const actions = {
  async ping() {
    return null;
  },
  async saveFilling(req) {
    const wxContext = cloud.getWXContext();
    const openid = wxContext.OPENID;

    const data = Object.assign(req, {
      openid,
      createdAt: new Date(),
    });

    const db = cloud.database();
    return db.collection("filling").add({
      data,
    });
  },
  async listFilling(req) {
    const wxContext = cloud.getWXContext();
    const openid = wxContext.OPENID;

    const db = cloud.database();
    return db
      .collection("filling")
      .where({
        openid,
      })
      .get();
  },
  async getFilling(req) {
    const db = cloud.database();
    return db
      .collection("filling")
      .where({
        _id: req.id,
      })
      .get();
  },
  async getQuestionnaire() {},
};
