const config = require("./config");
const themeListeners = [];
global.isDemo = true;

const backend = {
  async ping() {
    return wx.cloud.callFunction({
      name: "backend",
      data: { action: "ping" },
    });
  },
  async saveFilling(req) {
    return wx.cloud.callFunction({
      name: "backend",
      data: { action: "saveFilling", req },
    });
  },
  async listFilling(req) {
    return wx.cloud
      .callFunction({
        name: "backend",
        data: { action: "listFilling", req },
      })
      .then((resp) => {
        return resp.result.data.map((v) => new Report(v));
      });
  },
  async getFilling(req) {
    return wx.cloud
      .callFunction({
        name: "backend",
        data: { action: "getFilling", req },
      })
      .then((resp) => {
        return new Report(resp.result.data[0]);
      });
  },
  async getQuestionnaire(req) {
    return wx.cloud
      .callFunction({
        name: "backend",
        data: { action: "getQuestionnaire", req },
      })
      .then((resp) => resp.result);
  },
};

App({
  onLaunch(opts, data) {
    // const that = this;
    // const canIUseSetBackgroundFetchToken = wx.canIUse('setBackgroundFetchToken')
    // if (canIUseSetBackgroundFetchToken) {
    //   wx.setBackgroundFetchToken({
    //     token: 'getBackgroundFetchToken',
    //   })
    // }
    // if (wx.getBackgroundFetchData) {
    //   wx.getBackgroundFetchData({
    //     fetchType: 'pre',
    //     success(res) {
    //       that.globalData.backgroundFetchData  = res;
    //       console.log('读取预拉取数据成功')
    //     },
    //     fail() {
    //       console.log('读取预拉取数据失败')
    //       wx.showToast({
    //         title: '无缓存数据',
    //         icon: 'none'
    //       })
    //     },
    //     complete() {
    //       console.log('结束读取')
    //     }
    //   })
    // }
    console.log("App Launch", opts);
    if (data && data.path) {
      wx.navigateTo({
        url: data.path,
      });
    }

    wx.cloud.init({
      env: config.envId,
      traceUser: true,
    });
  },

  onShow(opts) {
    console.log("App Show", opts);
    // console.log(wx.getSystemInfoSync())
  },
  onHide() {
    console.log("App Hide");
  },
  onThemeChange({ theme }) {
    this.globalData.theme = theme;
    themeListeners.forEach((listener) => {
      listener(theme);
    });
  },
  watchThemeChange(listener) {
    if (themeListeners.indexOf(listener) < 0) {
      themeListeners.push(listener);
    }
  },
  unWatchThemeChange(listener) {
    const index = themeListeners.indexOf(listener);
    if (index > -1) {
      themeListeners.splice(index, 1);
    }
  },
  globalData: {
    theme: wx.getSystemInfoSync().theme,
    hasLogin: false,
    openid: null,
    iconTabbar: "/page/weui/example/images/icon_tabbar.png",
  },
  backend,
  // lazy loading openid
  getUserOpenId(callback) {
    const self = this;

    if (self.globalData.openid) {
      callback(null, self.globalData.openid);
    } else {
      wx.login({
        success(data) {
          wx.cloud.callFunction({
            name: "login",
            data: {
              action: "openid",
            },
            success: (res) => {
              console.log("拉取openid成功", res);
              self.globalData.openid = res.result.openid;
              callback(null, self.globalData.openid);
            },
            fail: (err) => {
              console.log(
                "拉取用户openid失败，将无法正常使用开放接口等服务",
                res
              );
              callback(res);
            },
          });
        },
        fail(err) {
          console.log(
            "wx.login 接口调用失败，将无法正常使用开放接口等服务",
            err
          );
          callback(err);
        },
      });
    }
  },
  // 通过云函数获取用户 openid，支持回调或 Promise
  getUserOpenIdViaCloud() {
    return wx.cloud
      .callFunction({
        name: "wxContext",
        data: {},
      })
      .then((res) => {
        this.globalData.openid = res.result.openid;
        return res.result.openid;
      });
  },
});

class Report {
  constructor(filling) {
    Object.assign(this, filling);

    this.title =
      this.name +
      "-" +
      new Date(this.createdAt).toLocaleDateString(undefined, {
        year: "numeric",
        month: "short",
        day: "numeric",
      });
  }

  values() {
    const ret = {};

    ret["姓名"] = this.form["姓名"];
    const curYear = new Date().getFullYear();
    const age = curYear - this.form["出生年份"];
    ret["年龄"] = age;
    const income = this.form["个人年收入"] + this.form["家庭其他年收入"];

    const sumExp =
      this.form["餐饮支出"] +
      this.form["服饰支出"] +
      this.form["不动产支出"] +
      this.form["交通支出"] +
      this.form["娱乐支出"] +
      this.form["通讯支出"] +
      this.form["养育支出"] +
      this.form["教育支出"] +
      this.form["人情支出"] +
      this.form["其他支出"];

    ret["月支出"] = sumExp;
    ret["每可月支配资金"] = income / 12 - sumExp;
    ret["个人收入"] = this.form["个人年收入"] / 12;
    ret["家庭其他月收入"] = this.form["家庭其他年收入"] / 12;
    ret["替代后生活水平"] = sumExp * 0.7;

    const workYear = this.form["预计退休年龄"] - age;
    ret["退休工资水平"] = ret["替代后生活水平"] * Math.pow(1.02, workYear);

    ret["退休期"] = this.form["预期寿命"] - this.form["预计退休年龄"];
    ret["退休金缺口"] =
      ret["退休工资水平"] - this.form["每月社保"] - this.form["养老金准备"];

    ret["退休金总需求"] = ret["退休金缺口"] * 12 * ret["退休期"];

    let familyCnt = 1;
    if (this.form["婚姻"] === 1 || this.form["婚姻"] === 2) familyCnt += 1;
    if (this.form["子女1出生年份"]) familyCnt += 1;
    if (this.form["子女2出生年份"]) familyCnt += 1;
    ret["家庭成员人数"] = familyCnt;

    ret["退休时储蓄"] = this.form["家庭储蓄"] * Math.pow(1.025, workYear);

    let saveings10 = 0;
    for (let i = 1; i <= 10; i++) {
      if (i > workYear) break;
      saveings10 += income * Math.pow(1 + this.form["10年涨幅"], i);
    }
    ret["未来十年收入"] = saveings10;

    let saveings15 = 0;
    const saveings15Base = income * Math.pow(1 + this.form["10年涨幅"], 10);
    for (let i = 1; i <= 5; i++) {
      if (i + 10 > workYear) break;
      saveings15 += saveings15Base * Math.pow(1 + this.form["三五涨幅"], i);
    }
    ret["未来三五收入"] = saveings15;

    let saveings20 = 0;
    const saveings20Base =
      saveings15Base * Math.pow(1 + this.form["三五涨幅"], 5);
    for (let i = 1; i <= 5; i++) {
      if (i + 15 > workYear) break;
      saveings20 += saveings20Base * Math.pow(1 + this.form["四五涨幅"], i);
    }
    ret["未来四五收入"] = saveings20;

    let saveingsMore = 0;
    const saveingsMoreBase =
      saveings20Base * Math.pow(1 + this.form["四五涨幅"], 5);
    if (workYear > 20) ret["退休前积蓄"] = saveingsMoreBase * (workYear - 20);

    return Object.entries(ret).map(([label, value]) => ({
      label,
      value,
    }));
  }
}

// D47 替代后生活水平   =I23*B47
// B47 养老生活替代率   =70%
// I23 月支出          =18600
// F47 通货率   =2%
// G8  预计退休年龄    =55
// B7  年龄           =31

// H47 退休工资水平     =D47*POWER((1+F47),(G8-B7))

// E48 预计寿命        =80
// B48 退休年龄        =55
// G48 退休期              =E48-B48

// B49 社保退休金     =12867
// G49 退休金缺口     =H47-B49-E49
// E49 已准备退休金/月=0
// I49 退休金总需求   =G49*12*G48/10000

// B50 家庭已有储蓄   =60
// G50 退休时储蓄 =B50*POWER(1.025,(G8-B7))
// B51 未来10年收入涨幅 =5%

// G52 未来1115年收入涨幅 =3%
// I52 未来1620年收入涨幅 =1%

// D51 未来十年积蓄 =
// (G6+G7)*(1+B51)*IF(1<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(2<=(G8-B7),1,0)*POWER((1+B51),IF(2<=(G8-B7),1,0))+
// (G6+G7)*(1+B51)*IF(3<=(G8-B7),1,0)*POWER((1+B51),IF(3<=(G8-B7),2,0))+
// (G6+G7)*(1+B51)*IF(4<=(G8-B7),1,0)*POWER((1+B51),IF(4<=(G8-B7),3,0))+
//  (G6+G7)*(1+B51)*IF(5<=(G8-B7),1,0)*POWER((1+B51),IF(5<=(G8-B7),4,0))+
//  (G6+G7)*(1+B51)*IF(6<=(G8-B7),1,0)*POWER((1+B51),IF(6<=(G8-B7),5,0))+
//  (G6+G7)*(1+B51)*IF(7<=(G8-B7),1,0)*POWER((1+B51),IF(7<=(G8-B7),6,0))+
//  (G6+G7)*(1+B51)*IF(8<=(G8-B7),1,0)*POWER((1+B51),IF(8<=(G8-B7),7,0))+
//  (G6+G7)*(1+B51)*IF(9<=(G8-B7),1,0)*POWER((1+B51),IF(9<=(G8-B7),8,0))+
//  (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))

// F51 未来三五积蓄 =
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(11<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(12<=(G8-B7),1,0)*POWER((1+G52),IF(12<=(G8-B7),1,0))+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(13<=(G8-B7),1,0)*POWER((1+G52),IF(13<=(G8-B7),2,0))+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(14<=(G8-B7),1,0)*POWER((1+G52),IF(14<=(G8-B7),3,0))+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))

// H51 未来四五年积蓄 =
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(16<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(17<=(G8-B7),1,0)*POWER((1+I52),IF(17<=(G8-B7),1,0))+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(18<=(G8-B7),1,0)*POWER((1+I52),IF(18<=(G8-B7),2,0))+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(19<=(G8-B7),1,0)*POWER((1+I52),IF(19<=(G8-B7),3,0))+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))

// J51 退休前积蓄 =
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(21<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(22<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(23<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(24<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(25<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(26<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(27<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(28<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(29<=(G8-B7),1,0)+
// (G6+G7)*(1+B51)*IF(10<=(G8-B7),1,0)*POWER((1+B51),IF(10<=(G8-B7),9,0))*(1+G52)*IF(15<=(G8-B7),1,0)*POWER((1+G52),IF(15<=(G8-B7),4,0))*(1+I52)*IF(20<=(G8-B7),1,0)*POWER((1+I52),IF(20<=(G8-B7),4,0))*IF(30<=(G8-B7),1,0)

// B52 持续收入储蓄 =
// D51+F51+H51+J51
// -(I23*12/10000*POWER((1+F47),IF(1<=(G8-B7),1,0))+
// I23*12/10000*POWER((1+F47),IF(2<=(G8-B7-1),2,0))+
// I23*12/10000*POWER((1+F47),IF(3<=(G8-B7-2),3,0))+
// I23*12/10000*POWER((1+F47),IF(4<=(G8-B7-3),4,0))+
// I23*12/10000*POWER((1+F47),IF(5<=(G8-B7-4),5,0))+
// I23*12/10000*POWER((1+F47),IF(6<=(G8-B7-5),6,0))+
// I23*12/10000*POWER((1+F47),IF(7<=(G8-B7-6),7,0))+
// I23*12/10000*POWER((1+F47),IF(8<=(G8-B7-7),8,0))+
// I23*12/10000*POWER((1+F47),IF(9<=(G8-B7-8),9,0))+
// I23*12/10000*POWER((1+F47),IF(10<=(G8-B7-9),10,0))+
// I23*12/10000*POWER((1+F47),IF(11<=(G8-B7-10),11,0))+
// I23*12/10000*POWER((1+F47),IF(12<=(G8-B7-11),12,0))+
// I23*12/10000*POWER((1+F47),IF(13<=(G8-B7-12),13,0))+
// I23*12/10000*POWER((1+F47),IF(14<=(G8-B7-13),14,0))+
// I23*12/10000*POWER((1+F47),IF(15<=(G8-B7-14),15,0))+
// I23*12/10000*POWER((1+F47),IF(16<=(G8-B7-15),16,0))+
// I23*12/10000*POWER((1+F47),IF(17<=(G8-B7-16),17,0))+
// I23*12/10000*POWER((1+F47),IF(18<=(G8-B7-17),18,0))+
// I23*12/10000*POWER((1+F47),IF(19<=(G8-B7-18),19,0))+
// I23*12/10000*POWER((1+F47),IF(20<=(G8-B7-19),20,0))+
// I23*12/10000*POWER((1+F47),IF(21<=(G8-B7-20),21,0))+
// I23*12/10000*POWER((1+F47),IF(22<=(G8-B7-21),22,0))+
// I23*12/10000*POWER((1+F47),IF(23<=(G8-B7-22),23,0))+
// I23*12/10000*POWER((1+F47),IF(24<=(G8-B7-23),24,0))+
// I23*12/10000*POWER((1+F47),IF(25<=(G8-B7-24),25,0))+
// I23*12/10000*POWER((1+F47),IF(26<=(G8-B7-25),26,0)))
