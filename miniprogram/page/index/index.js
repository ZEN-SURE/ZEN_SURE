Page({
  data: {
    // list: [
    //   {
    //     id: "start",
    //     name: "我的报告",
    //     open: false,
    //     pages: [{ name: "report", url: "/page/report-detail/report" }],
    //   },
    // ],
    reports: [],
    open: false,
    theme: "light",
  },

  onShareAppMessage() {
    return {
      title: "小程序官方组件展示",
      path: "page/component/index",
    };
  },
  onShareTimeline() {
    "小程序官方组件展示";
  },
  onLoad() {
    this.setData({
      theme: wx.getSystemInfoSync().theme || "light",
    });

    getApp()
      .backend.listFilling()
      .then((reports) => {
        this.setData({
          reports,
        });
      });
  },
  onShow() {
    if (wx.onThemeChange) {
      wx.onThemeChange(({ theme }) => {
        this.setData({ theme });
      });
    }
  },
  kindToggle(e) {
    this.setData({ open: !this.open });
    this.open = !this.open;
  },
});
