Page({
  data: {
    id: "",
    values: [],
  },
  report: null,
  onShareAppMessage() {
    return {
      title: "报告",
      path: "page/report-detail/report?id=" + this.data.id,
    };
  },
  onLoad(options) {
    console.log(options);
    const { id } = options;
    this.setData({ id });
    getApp()
      .backend.getFilling({ id })
      .then((report) => {
        this.report = report;
        this.setData({ values: report.values() });
      });
  },
});
