const formData = [
  {
    type: "input",
    id: "姓名",
    lable: "姓名",
    isRequired: true,
    maxLength: 20,
  },
  {
    type: "input",
    id: "出生年份",
    lable: "出生年份",
    isRequired: true,
    inputType: "number",
    placeholder: "1990",
    rules: [],
  },
  {
    type: "picker",
    id: "性别",
    lable: "性别",
    defaultIdx: 0,
    isRequired: true,
    range: [
      {
        id: 0,
        name: "保密",
      },
      {
        id: 1,
        name: "男",
      },
      {
        id: 2,
        name: "女",
      },
    ],
  },
  {
    type: "picker",
    id: "婚姻",
    lable: "婚姻",
    defaultIdx: 0,
    isRequired: true,
    range: [
      {
        id: 0,
        name: "单身",
      },
      {
        id: 1,
        name: "已婚未育",
      },
      {
        id: 2,
        name: "已婚已育",
      },
      {
        id: 3,
        name: "离异有子女",
      },
    ],
  },
  {
    type: "input",
    id: "个人年收入",
    lable: "个人年收入",
    isRequired: true,
    inputType: "number",
    placeholder: "500000",
    rules: [],
  },
  {
    type: "input",
    id: "家庭其他年收入",
    lable: "家庭其他年收入",
    isRequired: true,
    inputType: "number",
    placeholder: "配偶年收入、其他收入",
    rules: [],
  },
  {
    type: "input",
    id: "预计退休年龄",
    lable: "预计退休年龄",
    isRequired: true,
    inputType: "number",
    placeholder: "60",
    rules: [],
  },
  {
    type: "input",
    id: "子女1出生年份",
    lable: "子女1出生年份",
    isRequired: false,
    inputType: "number",
    placeholder: "若有",
    rules: [],
  },
  {
    type: "input",
    id: "子女2出生年份",
    lable: "子女2出生年份",
    isRequired: false,
    inputType: "number",
    placeholder: "若有",
    rules: [],
  },
  {
    type: "input",
    id: "餐饮支出",
    lable: "每月食品酒水",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "服饰支出",
    lable: "每月衣服服饰（全家）",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "不动产支出",
    lable: "每月居家物业（含房贷、水电煤）",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "交通支出",
    lable: "每月行车交通",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "娱乐支出",
    lable: "每月休闲娱乐",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "通讯支出",
    lable: "每月网络通讯",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "养育支出",
    lable: "每月子女养育（奶粉尿布营养品）",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "教育支出",
    lable: "每月子女教育",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "人情支出",
    lable: "每月人情往来",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "其他支出",
    lable: "每月其他",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "家庭储蓄",
    lable: "既有家庭储蓄",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "10年涨幅",
    lable: "家庭收入未来10年平均涨幅",
    defaultValue: 0,
    inputType: "digit",
    rules: [],
  },
  {
    type: "input",
    id: "三五涨幅",
    lable: "家庭收入未来10-15年平均涨幅",
    defaultValue: 0,
    inputType: "digit",
    rules: [],
  },
  {
    type: "input",
    id: "四五涨幅",
    lable: "家庭收入未来15-20年平均涨幅",
    defaultValue: 0,
    inputType: "digit",
    rules: [],
  },
  {
    type: "input",
    id: "预期寿命",
    lable: "预期寿命",
    defaultValue: 80,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "每月社保",
    lable: "每月预估社保养老",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "养老金准备",
    lable: "每月已准备养老金",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "身故风险",
    lable: "身故风险准备金",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "疾病风险",
    lable: "疾病风险准备金",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "教育金",
    lable: "教育准备金 高中、大学、继续深造",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "买车预算",
    lable: "买车预算",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "买车预算已准备",
    lable: "买车预算已准备",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "买车期限",
    lable: "买车需要准备年数",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "买房预算",
    lable: "买房预算",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "买房预算已准备",
    lable: "买房预算已准备",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "买房期限",
    lable: "买房需要准备年数",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "其他预算",
    lable: "其他预算",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "其他预算已准备",
    lable: "其他预算已准备",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },
  {
    type: "input",
    id: "其他期限",
    lable: "其他需要准备年数",
    defaultValue: 0,
    inputType: "number",
    rules: [],
  },

  // {
  //   type: "date",
  //   id: "timePicker",
  //   lable: "日期",
  //   isRequired: true,
  //   /* 显示完整时间包含时分秒；当使用endDate的时候关闭,不要同时打开, 否则日期将会换行；
  //      与config中的colum属性共同设置
  //   */
  //   // completeTime:true, //显示完整时间, 包含时分秒
  //   config: {
  //     endDate: true,
  //     dateLimit: true,
  //     // initStartTime: "2020-01-01 12:32:44",
  //     // initEndTime: "2020-12-01 12:32:44",
  //     column: "day", //day、hour、minute、secend
  //     limitStartTime: "2000-01-01 00:00:59",
  //     limitEndTime: "2100-01-01 00:00:59",
  //   },
  // },
  // {
  //   type: "textarea",
  //   id: "textarea1",
  //   lable: "描述",
  //   isRequired: true,
  //   maxLength: 200,
  //   // defaultValue: '初始值',
  //   placeholder: "请输入描述",
  //   rules: [
  //     {
  //       regular: "^.{5,200}$",
  //       tips: "请输入5-200位以内字符",
  //     },
  //   ],
  // },
];

const { backend } = getApp();

Page({
  data: {
    formData: formData,
    btnDisabled: false,
    toSubmit: Math.random(),
  },
  onShareAppMessage() {
    return {
      title: "问卷",
      path: "page/questionnaire/questionnaire",
    };
  },
  onFormSubmit(e) {
    const form = {};
    Object.entries(e.detail).forEach((entry) => {
      const [key, value] = entry;
      switch (value.original.type) {
        case "input":
          if (
            value.original.inputType === "number" ||
            value.original.inputType === "digit"
          ) {
            form[key] = Number(value.value);
            return;
          }
          form[key] = value.value;
          return;
        case "picker":
          form[key] = value.idx;
          return;
        default:
          throw new Error("todo");
      }
    });

    this.setData({
      btnDisabled: true,
    });

    backend
      .saveFilling({
        name: e.detail["姓名"].value,
        form,
        questionnaire: -1,
      })
      .then((resp) => {
        wx.navigateTo({
          url: "/page/report-detail/report?id=" + resp.result._id,
        });
      });
  },
  onFormChange(e) {
    // console.log("表单变化: ", e);
  },
  toSubmitChange() {
    this.setData({
      toSubmit: Math.random(),
    });
  },
  onLoad: function () {},
});
